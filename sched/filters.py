from jinja2 import Markup, escape, evalcontextfilter


def do_datetime(dt, format=None):
    if dt is None:
        return ''
        if format is None:
            formatted_date = dt.strftime('%Y-%m-%d - %A')
            formatted_time = dt.strftime('%I:%M%p').lstrip('0').lower()
            formatted = '%s at %s' % (formatted_date, formatted_time)
        else:
            formatted = dt.strftime(format)
        return formatted


def init_app(app):
    app.jinja_env.filters['datetime'] = do_datetime
    app.jinja_env.filters['date'] = do_date
    app.jinja_env.filters['duration'] = do_duration
    app.jinja_env.filters['nl2br'] = evalcontextfilter(do_nl2br)


def do_date(dt, format='%Y-%m-%d - %A'):
    if dt is None:
        return ''
    return dt.strftime(format)


def do_duration(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    d, h = divmod(h, 24)
    tokens = []
    if d > 1:
        tokens.append('{d} days')
    elif d:
        tokens.append('{d} day')
    if h > 1:
        tokens.append('{h} hours')
    elif h:
        tokens.append('{h} hour')
    if m > 1:
        tokens.append('{m} minutes')
    elif m:
        tokens.append('{m} minute')
    if s > 1:
        tokens.append('{s} seconds')
    elif s:
        tokens.append('{s} second')
    template = ', '.join(tokens)
    return template.format(d=d, h=h, m=m, s=s)


def do_nl2br(context, value):
    formatted = u'<br />'.join(escape(value).split('\n'))
    if context.autoescape:
        formatted = Markup(formatted)
    return formatted
