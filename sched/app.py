from flask import Flask, url_for, session
# -*- coding: utf-8 -*-
from flask.ext.sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask.ext.login import LoginManager, current_user
from flask.ext.login import login_user, logout_user

from flask import abort, jsonify, redirect, render_template
from flask import request

from sched.models import Base, Appointment, User
from sched.forms import AppointmentForm, LoginForm
from sched import filters

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///sched.db'

db = SQLAlchemy(app)
db.Model = Base

filters.init_app(app)

admin = Admin(app, name='agenda', template_mode='bootstrap3')
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Appointment, db.session))


login_manager = LoginManager()
login_manager.setup_app(app)
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).get(user_id)


@app.route('/')
def index():
    return render_template('/appointment/index.html')


@app.route('/appointments/')
def appointment_list():
    appts = (db.session.query(Appointment).
             order_by(Appointment.start.asc()).all())
    return render_template('appointment/list.html', appts=appts)


@app.route('/appointments/<int:appointment_id>/')
def appointment_detail(appointment_id):
    appt = db.session.query(Appointment).get(appointment_id)
    if appt is None:
        abort(404)
    return render_template('appointment/detail.html', appt=appt)


@app.route(
    '/appointments/<int:appointment_id>/edit/',
    methods=['GET', 'POST'])
def appointment_edit(appointment_id):
    appt = db.session.query(Appointment).get(appointment_id)
    if appt is None:
        abort(404)
    form = AppointmentForm(request.form, appt)
    if request.method == 'POST' and form.validate():
        form.populate_obj(appt)
        db.session.commit()
        return redirect(url_for('appointment_detail', appointment_id=appt.id))
    return render_template('appointment/edit.html', form=form)


@app.route('/appointments/create/', methods=['GET', 'POST'])
def appointment_create():
    form = AppointmentForm(request.form)
    if request.method == 'POST' and form.validate():
        appt = Appointment()
        form.populate_obj(appt)
        db.session.add(appt)
        db.session.commit()
        return redirect(url_for('appointment_list'))
    return render_template('appointment/edit.html', form=form)


@app.route('/appointments/<int:appointment_id>/delete/',
           methods=['DELETE'])
def appointment_delete(appointment_id):
    appt = db.session.query(Appointment).get(appointment_id)
    if appt is None:
        response = jsonify({'status': 'Not found'})
        response.status = 404
        return response
    db.session.delete(appt)
    db.session.commit()
    return jsonify({'status': 'OK'})


@app.errorhandler(404)
def error_not_found(error):
    return render_template('error/404.html'), 404


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('appointment_list'))
    form = LoginForm(request.form)
    error = None
    if request.method == 'POST' and form.validate():
        user = current_user
        login_user(user)
        return redirect(url_for('appointment_list'))
    else:
        error = 'Incorrect username or password. Try again.'
    return render_template('user/login.html', form=form, error=error)


@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('login'))

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
